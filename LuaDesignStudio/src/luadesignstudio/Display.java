/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package luadesignstudio;

import java.awt.List;
import java.util.ArrayList;
import java.util.ListIterator;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import my.luadesignstudio.Button;
import my.luadesignstudio.Text;
/**
 *
 * @author allen
 */
public class Display {

    private Display() {
        this.myButtons = new ArrayList<Button>();
        this.myTexts = new ArrayList<Text>();
    }
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JPanel jPanel3;

    public JPanel getjPanel2() {
        return jPanel2;
    }

    public void setjPanel2(JPanel jPanel2) {
        this.jPanel2 = jPanel2;
    }
    private javax.swing.JPanel jPanel2;
    private ArrayList<Button> myButtons;
    private ArrayList<Text> myTexts;
    private static Display display = null;
    
    
    public static Display getInstance(){
        if(display == null)
            display = new Display();
        return display;
    } 
    
    public JLayeredPane getjLayeredPane1() {
        return jLayeredPane1;
    }
    
    public void setjLayeredPane1(JLayeredPane jLayeredPane1) {
        this.jLayeredPane1 = jLayeredPane1;
    }

    public JPanel getjPanel3() {
        return jPanel3;
    }

    public void setjPanel3(JPanel jPanel3) {
        this.jPanel3 = jPanel3;
    }
    
    //buttons
    public void addButton(Button btn){
        System.out.println("added button to list");
        this.myButtons.add(btn);
    }
    
     public void removeButton(Button btn){
        this.myButtons.remove(btn);
        this.jLayeredPane1.remove(btn);
        this.jLayeredPane1.repaint();
        this.jPanel3.removeAll();
        this.jPanel3.repaint();

    }
     
     public ListIterator<Button> getButtons(){
         return this.myButtons.listIterator();
     }
     
     public int getButtonCount(){
         return this.myButtons.size();
     }
       
     //text
     public void addText(Text t){
         this.myTexts.add(t);
     }
     
     public void removeText(Text t){
         this.myTexts.remove(t);
         this.jLayeredPane1.remove(t);
         this.jLayeredPane1.repaint();
         this.jPanel3.removeAll();
         this.jPanel3.repaint();
     }
     
     public ListIterator<Text> getText(){
         return this.myTexts.listIterator();
     }
      
      public int getTextCount(){
         return this.myTexts.size();
     } 
      //clear the display
      public void clearDisplay(){
          this.jPanel3.removeAll();
          this.jPanel3.repaint();
          this.jLayeredPane1.removeAll();
          jLayeredPane1.add( this.jPanel2, jLayeredPane1.DEFAULT_LAYER, 1 );
          this.jLayeredPane1.repaint();
      }
    
}
