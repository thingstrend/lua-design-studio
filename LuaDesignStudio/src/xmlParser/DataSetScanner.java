/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xmlParser;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JPanel;
import luadesignstudio.Display;
import my.luadesignstudio.Button;
import my.luadesignstudio.Text;

/**
 *
 * @author allen
 */
public class DataSetScanner {
    /**
     * org.w3c.dom.Document document
     */
    org.w3c.dom.Document document;
    private Display display;

    /**
     * Create new DataSetScanner with org.w3c.dom.Document.
     */
    public DataSetScanner(org.w3c.dom.Document document) {
        this.document = document;
        this.display = Display.getInstance();
    }

    /**
     * Scan through org.w3c.dom.Document document.
     */
    public void visitDocument() {
        org.w3c.dom.Element element = document.getDocumentElement();
        if ((element != null) && element.getTagName().equals("object")) {
            visitElement_object(element);
        }
        if ((element != null) && element.getTagName().equals("buttons")) {
            visitElement_buttons(element);
        }
        if ((element != null) && element.getTagName().equals("button")) {
            visitElement_button(element);
        }
        if ((element != null) && element.getTagName().equals("texts")) {
            visitElement_texts(element);
        }
        if ((element != null) && element.getTagName().equals("text")) {
            visitElement_text(element);
        }
    }

    /**
     * Scan through org.w3c.dom.Element named object.
     */
    void visitElement_object(org.w3c.dom.Element element) {
        // <object>
        // element.getValue();
        org.w3c.dom.NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            org.w3c.dom.Node node = nodes.item(i);
            switch (node.getNodeType()) {
                case org.w3c.dom.Node.CDATA_SECTION_NODE:
                    // ((org.w3c.dom.CDATASection)node).getData();
                    break;
                case org.w3c.dom.Node.ELEMENT_NODE:
                    org.w3c.dom.Element nodeElement = (org.w3c.dom.Element) node;
                    if (nodeElement.getTagName().equals("buttons")) {
                        visitElement_buttons(nodeElement);
                    }
                    if (nodeElement.getTagName().equals("texts")) {
                        visitElement_texts(nodeElement);
                    }
                    break;
                case org.w3c.dom.Node.PROCESSING_INSTRUCTION_NODE:
                    // ((org.w3c.dom.ProcessingInstruction)node).getTarget();
                    // ((org.w3c.dom.ProcessingInstruction)node).getData();
                    break;
            }
        }
    }

    /**
     * Scan through org.w3c.dom.Element named buttons.
     */
    void visitElement_buttons(org.w3c.dom.Element element) {
        // <buttons>
        // element.getValue();
        org.w3c.dom.NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            org.w3c.dom.Node node = nodes.item(i);
            switch (node.getNodeType()) {
                case org.w3c.dom.Node.CDATA_SECTION_NODE:
                    // ((org.w3c.dom.CDATASection)node).getData();
                    break;
                case org.w3c.dom.Node.ELEMENT_NODE:
                    org.w3c.dom.Element nodeElement = (org.w3c.dom.Element) node;
                    if (nodeElement.getTagName().equals("button")) {
                        visitElement_button(nodeElement);
                    }
                    break;
                case org.w3c.dom.Node.PROCESSING_INSTRUCTION_NODE:
                    // ((org.w3c.dom.ProcessingInstruction)node).getTarget();
                    // ((org.w3c.dom.ProcessingInstruction)node).getData();
                    break;
            }
        }
    }

    /**
     * Scan through org.w3c.dom.Element named button.
     */
    void visitElement_button(org.w3c.dom.Element element) {
        // <button>
        // element.getValue();
        Button button = new Button();
        org.w3c.dom.NamedNodeMap attrs = element.getAttributes();
        int backRGB1 = 0, backRGB2 = 0, backRGB3 = 0;//background rgb values
        int labelRGB1 = 0, labelRGB2 = 0, labelRGB3 = 0;//label rgb values
        int width = 0, height = 0;//button width and height
        int x = 0, y = 0;
        for (int i = 0; i < attrs.getLength(); i++) {
            org.w3c.dom.Attr attr = (org.w3c.dom.Attr) attrs.item(i);
            
            if (attr.getName().equals("onEvent")) {
                button.setOnEvent(element.getAttribute("onEvent"));
//                  System.out.println( element.getAttribute("onEvent"));
            }

            if (attr.getName().equals("label_rgb1")) {
                labelRGB1 = Integer.parseInt(element.getAttribute("label_rgb1"));
            }
            if (attr.getName().equals("label_rgb2")) {
                labelRGB2 = Integer.parseInt(element.getAttribute("label_rgb2"));
            }
            if (attr.getName().equals("label_rgb3")) {
                labelRGB3 = Integer.parseInt(element.getAttribute("label_rgb3"));
            }
            if (attr.getName().equals("label")) {
                button.setLabel(element.getAttribute("label"));
            }
            if (attr.getName().equals("y")) {
                y = Integer.parseInt(element.getAttribute("y"));
            }
            if (attr.getName().equals("x")) {
                x = Integer.parseInt(element.getAttribute("x"));
            }
            if (attr.getName().equals("background_rgb1")) {
                   backRGB1 = Integer.parseInt(element.getAttribute("background_rgb1"));
            }  
            if (attr.getName().equals("background_rgb2")) {
                   backRGB2 = Integer.parseInt(element.getAttribute("background_rgb2"));
            } 
            if (attr.getName().equals("background_rgb3")) {
                   backRGB3 = Integer.parseInt(element.getAttribute("background_rgb3"));
            } 
            if (attr.getName().equals("width")) {
                   width = Integer.parseInt(element.getAttribute("width"));
            } 
            if (attr.getName().equals("height")) {
                   height = Integer.parseInt(element.getAttribute("height"));
            } 
            if (attr.getName().equals("name")) {
                   button.setName(element.getAttribute("name"));
            } 
        }
        //set position
        Point position = new Point(x, y);
        button.setLocation(position);
        //make the button
        button.setSize(width, height);
        button.setColor(new Color(backRGB1, backRGB2, backRGB3));//set button background color
        button.setLabelColor(new Color(labelRGB1, labelRGB2, labelRGB3));//set label color
        button.setLocation(button.getX(), button.getY());
        this.display.getjLayeredPane1().add(button, new Integer(3));
        button.create(this.display.getjPanel3());
        org.w3c.dom.NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            org.w3c.dom.Node node = nodes.item(i);
            switch (node.getNodeType()) {
                case org.w3c.dom.Node.CDATA_SECTION_NODE:
                    // ((org.w3c.dom.CDATASection)node).getData();
                    break;
                case org.w3c.dom.Node.ELEMENT_NODE:
                    org.w3c.dom.Element nodeElement = (org.w3c.dom.Element) node;
                    break;
                case org.w3c.dom.Node.PROCESSING_INSTRUCTION_NODE:
                    // ((org.w3c.dom.ProcessingInstruction)node).getTarget();
                    // ((org.w3c.dom.ProcessingInstruction)node).getData();
                    break;
            }
        }
    }

    /**
     * Scan through org.w3c.dom.Element named text.
     */
    void visitElement_texts(org.w3c.dom.Element element) {
        // <text>
        // element.getValue();
        org.w3c.dom.NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            org.w3c.dom.Node node = nodes.item(i);
            switch (node.getNodeType()) {
                case org.w3c.dom.Node.CDATA_SECTION_NODE:
                    // ((org.w3c.dom.CDATASection)node).getData();
                    break;
                case org.w3c.dom.Node.ELEMENT_NODE:
                    org.w3c.dom.Element nodeElement = (org.w3c.dom.Element) node;
                    if (nodeElement.getTagName().equals("text")) {
                        visitElement_text(nodeElement);
                    }
                    break;
                case org.w3c.dom.Node.PROCESSING_INSTRUCTION_NODE:
                    // ((org.w3c.dom.ProcessingInstruction)node).getTarget();
                    // ((org.w3c.dom.ProcessingInstruction)node).getData();
                    break;
            }
        }
    }

    /**
     * Scan through org.w3c.dom.Element named rectangle.
     */
    void visitElement_text(org.w3c.dom.Element element) {
        // <rectangle>
        // element.getValue();
        System.out.println("text");
        org.w3c.dom.NamedNodeMap attrs = element.getAttributes();
        Text text = new Text();
        int x = 0, y =0;
        int height = 0, width = 0;
        for (int i = 0; i < attrs.getLength(); i++) {
            org.w3c.dom.Attr attr = (org.w3c.dom.Attr) attrs.item(i);
            System.out.println(attr.getName());
            if (attr.getName().equals("align")) {
                text.setAlign(element.getAttribute("align"));
            }
            if (attr.getName().equals("y")) {
                 y = Integer.parseInt(element.getAttribute("y"));
            }
            if (attr.getName().equals("x")) {
               x = Integer.parseInt(element.getAttribute("x"));
            }
            if (attr.getName().equals("width")) {
                width = Integer.parseInt(element.getAttribute("width"));
            }
            if (attr.getName().equals("height")) {
               height = Integer.parseInt(element.getAttribute("height"));
            }
            if (attr.getName().equals("label")) {
                text.setText(element.getAttribute("label"));
            }
            if (attr.getName().equals("name")) {
                text.setName(element.getAttribute("name"));
            }
        }
        //set position
        System.out.println("Width: " + width);
        System.out.println("Height: " + height);
        Point position = new Point(x, y);
        text.setLocation(position);
        //make the text
        text.setSize(width, height);
        this.display.getjLayeredPane1().add(text, new Integer(3));
        text.create(this.display.getjPanel3());
        org.w3c.dom.NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            org.w3c.dom.Node node = nodes.item(i);
            switch (node.getNodeType()) {
                case org.w3c.dom.Node.CDATA_SECTION_NODE:
                    // ((org.w3c.dom.CDATASection)node).getData();
                    break;
                case org.w3c.dom.Node.ELEMENT_NODE:
                    org.w3c.dom.Element nodeElement = (org.w3c.dom.Element) node;
                    break;
                case org.w3c.dom.Node.PROCESSING_INSTRUCTION_NODE:
                    // ((org.w3c.dom.ProcessingInstruction)node).getTarget();
                    // ((org.w3c.dom.ProcessingInstruction)node).getData();
                    break;
            }
        }
    }
    
}
