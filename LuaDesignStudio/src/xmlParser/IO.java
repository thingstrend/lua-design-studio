/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xmlParser;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ListIterator;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import luadesignstudio.Display;
import my.luadesignstudio.Button;
import my.luadesignstudio.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author allen
 * @author Justin
 *
 */



//creating an xml doc http://www.mkyong.com/java/how-to-create-xml-file-in-java-dom/    
public class IO {

    public void loadXML(String contents) throws SAXException, IOException{
        System.err.println("here");
        Map<String, String> data;
         try {
                //build the xml doc file
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                InputStream inputStream = new ByteArrayInputStream(contents.getBytes());
                Document xmlFile = docBuilder.parse(inputStream);
                DataSetScanner scanner = new DataSetScanner(xmlFile);
                scanner.visitDocument();
                //data = createMapOfAttributeValuesKeyedByName(xmlFile.getDocumentElement());

         }
         catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } 
        // data= Collections.emptyMap();
        // return data;
         /*
         catch (TransformerException tfe) {
		tfe.printStackTrace();
	  }*/
    }
    
    public void saveXml(String path){
          try {
 
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                
                // root elements
		Document doc = docBuilder.newDocument();
                
                Element rootElement = doc.createElement("object");
                doc.appendChild(rootElement);
                Element buttonsElement = doc.createElement("buttons");
               
                rootElement.appendChild(buttonsElement);
                Display display = Display.getInstance();
                ListIterator<Button> theButtons = display.getButtons();
                for(ListIterator<Button> iter = theButtons; iter.hasNext();){
                    Button button = iter.next();
                    Element buttonElement = doc.createElement("button");
                    buttonElement.setAttribute("align", button.getOnEvent());
                    Color c = button.getColor();
                    buttonElement.setAttribute("name", button.getName());
                    buttonElement.setAttribute("background_rgb1", Integer.toString(c.getRed()));
                    buttonElement.setAttribute("background_rgb2", Integer.toString(c.getGreen()));
                    buttonElement.setAttribute("background_rgb3", Integer.toString(c.getBlue()));
                    buttonElement.setAttribute("label", button.getLabel());
                    Color l = button.getLabelColor();
                    buttonElement.setAttribute("label_rgb1", Integer.toString(l.getRed()));
                    buttonElement.setAttribute("label_rgb2", Integer.toString(l.getGreen()));
                    buttonElement.setAttribute("label_rgb3", Integer.toString(l.getBlue()));
                    buttonElement.setAttribute("x", Integer.toString(button.getX()));
                    buttonElement.setAttribute("y", Integer.toString(button.getY()));
                    buttonElement.setAttribute("width", Integer.toString(button.getWidth()));
                    buttonElement.setAttribute("height", Integer.toString(button.getHeight()));
                    buttonsElement.appendChild(buttonElement);
                }
                 
                
                //loop through buttons here
                Element thetextElement = doc.createElement("texts");
                rootElement.appendChild(thetextElement);
                ListIterator<Text> theText = display.getText();
                  for(ListIterator<Text> iter = theText; iter.hasNext();){
                    Text text = iter.next();
                    Element textElement = doc.createElement("text");

//                    Color c = text.getColor();
//                    textElement.setAttribute("background_rgb1", Integer.toString(c.getRed()));
//                    textElement.setAttribute("background_rgb2", Integer.toString(c.getGreen()));
//                    textElement.setAttribute("background_rgb3", Integer.toString(c.getBlue()));
                    textElement.setAttribute("label", text.getText());
                    textElement.setAttribute("align", text.getAlign());
                    textElement.setAttribute("x", Integer.toString(text.getX()));
                    textElement.setAttribute("y", Integer.toString(text.getY()));
                    textElement.setAttribute("width", Integer.toString(text.getWidth()));
                    textElement.setAttribute("height", Integer.toString(text.getHeight()));
                    thetextElement.appendChild(textElement);
                }
                
                // write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(path));
 
		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);
 
		transformer.transform(source, result);
 
		System.out.println("File saved!");
            }
         catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } catch (TransformerException tfe) {
		tfe.printStackTrace();
	  }
    }

    public void export(String path) throws IOException
    {
        ListIterator<Button> theButtons = Display.getInstance().getButtons();
        ListIterator<Text> theText = Display.getInstance().getText();
        try
        {
            PrintWriter writer = new PrintWriter(new FileWriter(path));
            String luaCode = "local widget = require( \"widget\" )\n";
            luaCode += "widget.setTheme(\"widget_theme_ios\" )\n\n";

            for(ListIterator<Button> iter = theButtons; iter.hasNext();)
            {    
                Button button = iter.next();
                luaCode += button.export() + "\n\n";
            }
            
             for(ListIterator<Text> iter = theText; iter.hasNext();)
	    {
		Text text = iter.next();
		luaCode += text.export() + "\n\n";
	    }

            writer.write(luaCode);
            writer.close();
        }

        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
            
}
