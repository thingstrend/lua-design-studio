/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package my.luadesignstudio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import luadesignstudio.Display;

/**
 *
 * @author Matthew
 */
public class Text extends JLabel implements Component {
    private String name;
    private String text;
    private int width;
    private int height;
    private int x;
    private int y;
    private int size;
    private String label;
    private String align;

    // for making draggable
    protected Point anchorPoint;
    protected Cursor draggingCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
    
    public Text() {
        super();
        addDragListeners();
        
        name = "text";
        setSize( 50, 35 );
        setText( "text" );
        label = "text";
        Display.getInstance().addText(this);
        
        this.addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                create(Display.getInstance().getjPanel3());
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
           });
    }

    @Override
    public void create( JPanel parent ) {
        parent.removeAll(); //clear the panel so we can get to the new form
        JLabel nameLabel        = new JLabel("Name: ");
        JLabel textLabel        = new JLabel("Label: ");
        JLabel alignLabel     = new JLabel("Align: ");
        JLabel widthLabel       = new JLabel("Width: ");
        JLabel heightLabel      = new JLabel("Height: ");
        
        final JTextField nameField       = new JTextField();
        final JTextField textField      = new JTextField();
        final JTextField alignField    = new JTextField();
        final JTextField widthField      = new JTextField();
        final JTextField heightField     = new JTextField();
        
        nameLabel.setSize( 70, 20 );
        nameLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 10 );
        
        textLabel.setSize( 70, 20 );
        textLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 45 );
        
        alignLabel.setSize( 70, 20 );
        alignLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 80 );
        
        widthLabel.setSize( 70, 20 );
        widthLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 115 );
        
        heightLabel.setSize( 70, 20 );
        heightLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 150 );
        
        nameField.setSize( 150, 30 );
        nameField.setLocation( nameLabel.getLocation().x + nameLabel.getWidth() + 10, nameLabel.getLocation().y - 5 );
        nameField.setText( name );
        
        textField.setSize( 150, 30 );
        textField.setLocation( textLabel.getLocation().x + textLabel.getWidth() + 10, textLabel.getLocation().y - 5 );
        textField.setText( text );
            
        alignField.setSize( 150, 30 );
        alignField.setLocation( alignLabel.getLocation().x + alignLabel.getWidth() + 10, alignLabel.getLocation().y - 5 );
        alignField.setText( align );
        
        widthField.setSize( 150, 30 );
        widthField.setLocation( widthLabel.getLocation().x + widthLabel.getWidth() + 10, widthLabel.getLocation().y - 5 );
        widthField.setText( Integer.toString( width ) );
        
        heightField.setSize( 150, 30 );
        heightField.setLocation( heightLabel.getLocation().x + heightLabel.getWidth() + 10, heightLabel.getLocation().y - 5 );
        heightField.setText( Integer.toString( height ) );
        JButton delButton = new JButton("Delete");
        delButton.setSize(100, 30);
        delButton.setLocation(heightLabel.getLocation().x + 50, heightLabel.getLocation().y + 30);
        parent.add(delButton);
        parent.repaint();
        
        final Text handle = this;

        delButton.addActionListener(new ActionListener() {

            //Handle JButton event if Enter key is pressed or if mouse is clicked.  
            public void actionPerformed(ActionEvent event) {
                System.out.println("here");
                Display display = Display.getInstance();
                display.removeText(handle);
                
            }
        });
        parent.add( nameLabel );
        parent.add( textLabel );
        parent.add( alignLabel );
        parent.add( widthLabel );
        parent.add( heightLabel );
        
        parent.add( nameField );
        parent.add( textField );
        parent.add( alignField );
        parent.add( widthField );
        parent.add( heightField );
        
        parent.repaint();
        
        nameField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                name = nameField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                name = nameField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        textField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                text = textField.getText();
                setText(text);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                text = textField.getText();
                setText(text);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
                
        alignField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                align = alignField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                align = alignField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        widthField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if( !widthField.getText().equals( "" ) )
                    setSize( Integer.parseInt( widthField.getText() ), height );
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if( !widthField.getText().equals( "" ) )
                    setSize( Integer.parseInt( widthField.getText() ), height );
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        heightField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if( !heightField.getText().equals( "" ) )
                    setSize( width, Integer.parseInt( heightField.getText() ) );
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if( !heightField.getText().equals( "" ) )
                    setSize( width, Integer.parseInt( heightField.getText() ) );
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    @Override
    public void edit() {

    }

    @Override
    public void delete() {

    }

     @Override
    public String export() 
    {

            int x = getX();
            int y = getY();
            String name = getName();
            String label = getLabel();
            int size = getFontSize(); 
            return "\nlocal " + name + "= display.newText(\"" + label + "\"," + getX() + "," + getY() + "," + "native.systemFont," + size + ")\n";
    }
     
    public String getName(){
        return name;
    }
    public String getLabel() {
        return label;
    }
    
    
    @Override
    public String getText() {
        return text;
    }
    
     public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public int getFontSize() {
        return size;
    }

    @Override
    public void setText(String theText) {
        text = theText;
        label = theText;
        super.setText(theText);
    }

    @Override
    public int getWidth() {
        return width;
    }

    public void setWidth(int theWidth) {
        width = theWidth;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public void setHeight(int theHeight) {
        height = theHeight;
    }

    @Override
    public void setSize( int theWidth, int theHeight ) {
        height = theHeight;
        width = theWidth;
        super.setSize(theWidth, theHeight);
    }
    
    @Override
    public void setX(int theX) {
        x = theX;
    }

    @Override
    public void setY(int theY) {
        y = theY;
    }
    
    @Override
    public void setLocation( int x, int y ) {
        // location is left top based not center
        setX( x + getWidth()/2 );
        setY( y + getHeight()/2 );
        super.setLocation( x, y );
    }
    
    @Override
    public void setLocation( Point point ) {
        setLocation( point.x, point.y );
    }
//    
    private void addDragListeners() {
        /** This handle is a reference to THIS because in next Mouse Adapter 
	"this" is not allowed */
        final Component handle = this;
        addMouseMotionListener(new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {
                anchorPoint = e.getPoint();
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                int anchorX = anchorPoint.x;
                int anchorY = anchorPoint.y;

                Point parentOnScreen = getParent().getLocationOnScreen();
                Point mouseOnScreen = e.getLocationOnScreen();
                Point position = new Point(mouseOnScreen.x - parentOnScreen.x - 
		anchorX, mouseOnScreen.y - parentOnScreen.y - anchorY);
                setLocation(position);
            }
        });
    }
}

