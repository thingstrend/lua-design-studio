/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package my.luadesignstudio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import luadesignstudio.Display;

/**
 *
 * @author Matthew
 * @author Justin
 *
 */

public class Button extends JButton implements Widget {
    private int x;
    private int y;
    private int width;
    private int height;
    private String name;
    private Color color;
    private String label;
    private Color labelColor;
    private String onEvent;
    
    // for making draggable
    protected Point anchorPoint;
    protected Cursor draggingCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
    
    public Button() {
        super();
        addDragListeners();
        setLabel("text");
        setSize( 50, 35 );
        name = "Button";
        color = Color.white;
        labelColor = Color.black;
        onEvent = "";
        setBackground(color);
        //add button to list
        System.out.println("Button");
        Display display = Display.getInstance();
        display.addButton(this);
        //bind event so we can edit the button
        this.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                   Display display = Display.getInstance();
                   System.out.println("click");
                   create(display.getjPanel3());
                }
           });           
    }
    
    @Override
    public void create( JPanel parent ) {
        parent.removeAll(); //clear the panel so we can get to the new form
        JLabel nameLabel        = new JLabel("Name: ");
        JLabel labelLabel       = new JLabel("Label: ");
        JLabel colorLabel       = new JLabel("Color: ");
        JLabel labelColorLabel  = new JLabel("Label Color: ");
        JLabel onEventLabel     = new JLabel("On Event: ");
        JLabel widthLabel       = new JLabel("Width: ");
        JLabel heightLabel      = new JLabel("Height: ");
        
        final JTextField nameField       = new JTextField();
        final JTextField labelField      = new JTextField();
        final JTextField colorField      = new JTextField();
        final JTextField labelColorField = new JTextField();
        final JTextField onEventField    = new JTextField();
        final JTextField widthField      = new JTextField();
        final JTextField heightField     = new JTextField();
        
        nameLabel.setSize( 70, 20 );
        nameLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 10 );
        
        labelLabel.setSize( 70, 20 );
        labelLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 45 );
        
        colorLabel.setSize( 70, 20 );
        colorLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 80 );
        
        labelColorLabel.setSize( 70, 20 );
        labelColorLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 115 );
        
        onEventLabel.setSize( 70, 20 );
        onEventLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 150 );
        
        widthLabel.setSize( 70, 20 );
        widthLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 185 );
        
        heightLabel.setSize( 70, 20 );
        heightLabel.setLocation( parent.getLocation().x + 10, parent.getLocation().y + 220 );
        
        nameField.setSize( 150, 30 );
        nameField.setLocation( nameLabel.getLocation().x + nameLabel.getWidth() + 10, nameLabel.getLocation().y - 5 );
        nameField.setText( name );
        
        labelField.setSize( 150, 30 );
        labelField.setLocation( labelLabel.getLocation().x + labelLabel.getWidth() + 10, labelLabel.getLocation().y - 5 );
        labelField.setText( label );
        
        colorField.setSize( 150, 30 );
        colorField.setLocation( colorLabel.getLocation().x + colorLabel.getWidth() + 10, colorLabel.getLocation().y - 5 );
        colorField.setText( "[" + Integer.toString( color.getRed() ) + "," + Integer.toString( color.getGreen() ) + "," + Integer.toString( color.getBlue() ) + "]" );
        
        labelColorField.setSize( 150, 30 );
        labelColorField.setLocation( labelColorLabel.getLocation().x + labelColorLabel.getWidth() + 10, labelColorLabel.getLocation().y - 5 );
        labelColorField.setText( "[" + Integer.toString( labelColor.getRed() ) + "," + Integer.toString( labelColor.getGreen() ) + "," + Integer.toString( labelColor.getBlue() ) + "]" );
              
        onEventField.setSize( 150, 30 );
        onEventField.setLocation( onEventLabel.getLocation().x + onEventLabel.getWidth() + 10, onEventLabel.getLocation().y - 5 );
        onEventField.setText( onEvent );
        
        widthField.setSize( 150, 30 );
        widthField.setLocation( widthLabel.getLocation().x + widthLabel.getWidth() + 10, widthLabel.getLocation().y - 5 );
        widthField.setText( Integer.toString( width ) );
        
        heightField.setSize( 150, 30 );
        heightField.setLocation( heightLabel.getLocation().x + heightLabel.getWidth() + 10, heightLabel.getLocation().y - 5 );
        heightField.setText( Integer.toString( height ) );
        
        parent.add( nameLabel );
        parent.add( labelLabel );
        parent.add( colorLabel );
        parent.add( labelColorLabel );
     
        parent.add( onEventLabel );
        parent.add( widthLabel );
        parent.add( heightLabel );
        
        parent.add( nameField );
        parent.add( labelField );
        parent.add( colorField );
        parent.add( labelColorField );

        parent.add( onEventField );
        parent.add( widthField );
        parent.add( heightField );
        JButton delButton = new JButton("Delete");
        delButton.setSize(100, 30);
        delButton.setLocation(heightLabel.getLocation().x + 50, heightLabel.getLocation().y + 30);
        parent.add(delButton);
        parent.repaint();
        final Button handle = this;

        delButton.addActionListener(new ActionListener() {

            //Handle JButton event if Enter key is pressed or if mouse is clicked.  
            public void actionPerformed(ActionEvent event) {
                System.out.println("here");
                Display display = Display.getInstance();
                display.removeButton(handle);
                
            }
        });
        nameField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                name = nameField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                name = nameField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        labelField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                label = labelField.getText();
                setText(label);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                label = labelField.getText();
                 setText(label);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        colorField.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                Color newColor = JColorChooser.showDialog( new JColorChooser(Color.white), "Pick a background color.", Color.white );
                colorField.setText( "[" + Integer.toString( newColor.getRed() ) + "," + Integer.toString( newColor.getGreen() ) + "," + Integer.toString( newColor.getBlue() ) + "]" );
                color = newColor;
                setBackground(newColor);

            }
        });
        
        labelColorField.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                Color newColor = JColorChooser.showDialog( new JColorChooser(Color.white), "Pick a background color.", Color.white );
                labelColorField.setText( "[" + Integer.toString( newColor.getRed() ) + "," + Integer.toString( newColor.getGreen() ) + "," + Integer.toString( newColor.getBlue() ) + "]" );
                labelColor = newColor;
                setForeground(newColor);

            }
        });
                
        onEventField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                onEvent = onEventField.getText();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onEvent = onEventField.getText();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        widthField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if( !widthField.getText().equals( "" ) )
                    setSize( Integer.parseInt( widthField.getText() ), height );
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if( !widthField.getText().equals( "" ) )
                    setSize( Integer.parseInt( widthField.getText() ), height );
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        heightField.getDocument().addDocumentListener( new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if( !heightField.getText().equals( "" ) )
                    setSize( width, Integer.parseInt( heightField.getText() ) );
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if( !heightField.getText().equals( "" ) )
                    setSize( width, Integer.parseInt( heightField.getText() ) );
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    @Override
    public void edit() {

    }

    @Override
    public void delete() {

    }

    public String getName() {
        return name;
    }

    @Override
    public String export()
    {
        int x = getX();
        int y = getY();
        String name = getName();
        Color color = getColor();
        String label = getLabel();
        Color labelColor = getLabelColor();
        String onEvent = this.getOnEvent();

        String luaCode = "";
        
        luaCode += "local function " + onEvent + "( event )\n";
        luaCode += "\tif ( event.phase == \"ended\" ) then\n\n";
        luaCode += "\tend\n";
        luaCode += "end\n\n";
        
        luaCode += "local " + name + " = widget.newButton\n";
        luaCode += "{\n";
        luaCode += "\tid = " + name + ",\n";
        luaCode += "\tlabel = \"" + label + "\",\n";
        luaCode += "\tlabelColor = { default={" + Integer.toString(labelColor.getRed()) + "," + Integer.toString(labelColor.getGreen()) + "," + Integer.toString(labelColor.getBlue()) + "}},\n";
        luaCode += "\tonEvent = " + onEvent + ",\n";
        luaCode += "}\n";
        luaCode += name + ":setFillColor(" + Double.toString(color.getRed()/255.0) + "," + Double.toString(color.getGreen()/255.0) + "," + Double.toString(color.getBlue()/255.0) + ")\n";
        luaCode += name + ".x = " + String.valueOf(x) + "\n";
        luaCode += name + ".y = " + String.valueOf(y) + "\n";
        
        return luaCode;
    }
    
    @Override
    public void setSize( int theWidth, int theHeight ) {
        width = theWidth;
        height = theHeight;
        super.setSize( width, height );
    }
    
    public Color getColor() {
        return color;
    }

    public void setColor(Color theColor) {
        color = theColor;
        super.setBackground(color);
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String theLabel) {
        label = theLabel;
        setText( theLabel );
    }   
    
    public void setLabelColor(Color theLabelColor) {
        labelColor = theLabelColor;
         super.setForeground(labelColor);
    }
    
    public Color getLabelColor() {
       return labelColor;
    }



    public String getOnEvent() {
        return onEvent;
    }

    public void setOnEvent(String theEvent) {
        onEvent = theEvent;
    }

    @Override
    public void setX(int theX) {
        x = theX;
    }

    @Override
    public void setY(int theY) {
        y = theY;
    }
    
    @Override
    public void setLocation( int x, int y ) {
        // location is left top based not center
        setX( x + getWidth()/2 );
        setY( y + getHeight()/2 );
        super.setLocation( x, y );
    }
    
    @Override
    public void setLocation( Point point ) {
        setLocation( point.x, point.y );
    }
    
    private void addDragListeners() {
        final Widget handle = this;
        addMouseMotionListener(new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {
                anchorPoint = e.getPoint();
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                int anchorX = anchorPoint.x;
                int anchorY = anchorPoint.y;

                Point parentOnScreen = getParent().getLocationOnScreen();
                Point mouseOnScreen = e.getLocationOnScreen();
                Point position = new Point(mouseOnScreen.x - parentOnScreen.x - 
		anchorX, mouseOnScreen.y - parentOnScreen.y - anchorY);
                setLocation(position);
            }
        });
    }
}

