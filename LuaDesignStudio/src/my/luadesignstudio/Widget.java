/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package my.luadesignstudio;

import java.io.IOException;
import javax.swing.JPanel;

/**
 *
 * @author Matthew
 */
public interface Widget {
    public void create(JPanel parent);
    public void edit();
    public void delete();
    public String export();
    public int getX();
    public void setX(int theX);
    public int getY();
    public void setY(int theY);
    public String getOnEvent();
    public void setOnEvent(String s);
}
